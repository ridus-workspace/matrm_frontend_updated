import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { observable, throwError} from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root',
})
export class LoginService {
  headers = new HttpHeaders().set("Content-Type", "application/json");
  currentUser = {};
  constructor(
    private http: HttpClient, 
    public router: Router
    ){ }

  logIn(user){

    return this.http.post<any>(environment.baseUrl + '/login', user);
  
  }

  
}
