import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRouterModule } from './login-router.module';
import { LoginComponent } from './login.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
// import { MatButton } from '@angular/material/button';
// import { MatCheckbox } from '@angular/material/checkbox';
// import { MatInput} from '@angular/material/input';
// import { MatSnackBar } from '@angular/material/snack-bar';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRouterModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule
    // MatButton,
    // MatCheckbox,
    // MatInput,
    // MatSnackBar
  ]
})
export class LoginModule { }
