import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { LoginService } from './login.service';
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private loginService: LoginService,
    private _snackBar: MatSnackBar
  ) {
    this.loginForm = this.fb.group({
      email: ["",Validators.required],
      password: ["",Validators.required]
    });
   }

  ngOnInit() { }

  get f() {
    // console.log(this.loginForm.controls);
    return this.loginForm.controls;
  }

  onLogin(){
    
    const data = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };
    this.submitted = true;
    
   this.loginService.logIn(data).subscribe((res: any) => {
    localStorage.setItem("access_token", res.success.data.token);
    localStorage.setItem("user_name", res.success.data.name);
    localStorage.setItem("user_id", res.success.data.userId);
    this.router.navigate(["/home"]);
   },
   (error) => {
    const msg = error["error"].error.message;
        this._snackBar.open(msg, "", {
          duration: 3000,
        });
   });

  }

}
